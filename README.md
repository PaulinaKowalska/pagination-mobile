# Pagination

## Installation
1. Clone repository
2. Install dependencies - `npm install`
3. Build project using webpack - `npm run webpack`
4. Run watcher for server - `npm run server-watch`
5. Website will be available on `localhost:8080`