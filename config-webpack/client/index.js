'use strict';

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const extractLESS = new ExtractTextPlugin('../css/main.css');

module.exports = {
    entry: [
        path.resolve(__dirname, '../../src/client/less') + '/index.less',
        path.resolve(__dirname, '../../src/client/js') + '/index.js',
        'babel-polyfill'
    ],
    output: {
        path: path.join(__dirname, '../../dist/client/js'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                exclude: /node_modules/,
                test: /\.js$/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.(png)$/, loader: 'url-loader?limit=100000'
            },
            {
                test: /\.less$/,
                use: extractLESS.extract(['css-loader', 'less-loader'])
            }
        ]
    },
    plugins: [
        extractLESS
    ]
};