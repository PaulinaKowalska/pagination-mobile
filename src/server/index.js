//Import necessary libraries
import express from 'express';
import path from 'path';

import getArticles from './api/getArticles';

//Create server by express
const app = express();
const publicDir = (path.join(process.cwd(), '/dist/client'));
const port = 8080;

//Serve folder with static files
app.use('/', express.static(publicDir));

//Serve static HTML file
app.get('/', function (req, res) {
    res.sendFile(path.resolve(process.cwd() + '/index.html'));
});

//Endpoint get articles
app.get('/api/articles/:articles_iterator', (req, res) => getArticles(req, res));

//Listening on port
app.listen(port, err => {
    if(err) console.error(err);
    console.log(`Server is running on port ${port}...`);
});