import {dataObject} from './../utils/loadData';

export default function getArticles(req, res) {
    const pageNumber = parseInt(req.params.articles_iterator);
    const startSection = pageNumber * 4; // select articles from startSection
    const endSection = startSection + 4; // select articles to endSection
    const amountOfArticles = dataObject.elements.length;
    const numberOfPages = (amountOfArticles % 4) === 0 ? Math.floor(amountOfArticles / 4) : Math.floor(amountOfArticles / 4) + 1;
    const resultArticles = [];  // array to put articles for response
    if (endSection <= amountOfArticles) {
        for (let i = startSection; i < endSection; i++) {
            resultArticles.push(dataObject.elements[i]); // push correct articles
        }
    } else {
        for (let i = startSection; i < amountOfArticles; i++) {
            resultArticles.push(dataObject.elements[i]); // push correct articles
        }
    }
    const isLast = pageNumber === numberOfPages - 1; // check if next page is last
    const responseObject = {
        isLast,
        articles: resultArticles
    };
    res.send(responseObject);
}