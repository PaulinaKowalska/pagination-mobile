import fs from 'fs-extra';
// load data from JSON and parse into JavaScript object
const dataString = JSON.stringify(fs.readJsonSync('data/data.json'));
const dataObject = JSON.parse(dataString);

export  { dataObject };