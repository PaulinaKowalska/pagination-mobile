import {articlesContainer} from './../elementDOM/index';

export default function addArticle(data) {
    articlesContainer.innerHTML +=
        `<article>
            <div class="article-image">
                <img src="${data.image}"
                     alt="article image">
                <span class="icon ${data.type === "Gallery" ? "icon-photo" : data.type === "Video" ? "icon-play" : ""}">
                </span>
            </div>
            <div class="article-description">
                <p>
                    ${data.title}
                </p>
                <small>Kraków</small>
            </div>
        </article>`;
}