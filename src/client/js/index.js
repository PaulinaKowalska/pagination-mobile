import showMoreButtonHandler from './handlers/showMoreButtonHandler';
import {showMoreButton} from './elementDOM/index';

/* Run when DOM is loaded */
document.addEventListener('DOMContentLoaded', function () {
    let i = 0;
    function* pageIterator() {
        while (true) {
            yield i++;
        }
    }
    let iterator = pageIterator();
    showMoreButtonHandler(iterator);
    showMoreButton.addEventListener('click', showMoreButtonHandler.bind(this, iterator));
});