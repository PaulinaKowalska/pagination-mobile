import {showMoreButton} from './../elementDOM/index';
import addArticle from './../utils/addArticle';

export default function showMoreButtonHandler(iterator) {
    fetch(`http://localhost:8080/api/articles/${iterator.next().value}`)
        .then(res => res.json())
        .then(data => {
            if (data.isLast) {
                showMoreButton.style.display = 'none';
            }
            for (let i = 0; i < data.articles.length; i++) {
                addArticle(data.articles[i]);
            }
        })
        .catch(err => {
            console.log(err);
        });
};